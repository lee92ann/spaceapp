import React, {Fragment} from 'react';
import { Navbar, NavbarBrand} from 'reactstrap';

function Header() {
    return(
        <Fragment>
            <Navbar expand="md">
                <div className = 'container'>
                    <NavbarBrand className="mr-auto" href="/">
                        <img src="assets/images/logo.PNG" height="30" width="41" alt="logo"/>
                    </NavbarBrand>
                </div>
            </Navbar>
        </Fragment>
    );
}

export default Header;


