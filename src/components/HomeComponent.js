import React, { Component } from 'react';
import {Modal, Button, Card, CardText, CardBody, CardSubtitle, CardTitle, ModalHeader, ModalBody, Label, Row,Col } from 'reactstrap';
import { Control, LocalForm, Errors } from 'react-redux-form';

const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => val && (val.length >= len);

class CommentForm extends Component {

    constructor(props){
        super(props);

        this.state = {
            isModalOpen: false,
            missionId: "",
            secondAstronautsGuId: "",
            firstAstronautsGuId: ""
        }

        this.toggleModal = this.toggleModal.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.resetState = this.resetState.bind(this);
    }

    handleSubmit(values) {
        this.toggleModal();

        this.callBackendAPI(values)
        .then(res => this.setState({
            missionId: res.missionId,
            firstAstronautsGuId: res.firstAstronautsGuId,
            secondAstronautsGuId: res.secondAstronautsGuId
        })).catch(err => console.log(err));

        }
        callBackendAPI = async (values) => {
            const response = await fetch('/launch/' + values.guId);
            const body = await response.json();

            if (response.status === 404) {
              return(
                alert("Problem with request, have you provided the correct GuId?")
              )
            }
            return (
                body
            );
        };

    toggleModal() {
        this.setState({
            isModalOpen: !this.state.isModalOpen
        });
    }

    resetState() {
        this.setState({
            missionId: "",
            secondAstronautsGuId: "",
            firstAstronautsGuId: ""
        });
    }

    render(){
        return(
            <div>
                <div>
                    {(!this.state.missionId) ? <Button className=" launchButton" onClick={this.toggleModal} block><h1> Launch It Now</h1></Button>: <div >
                        <Card>
                            <CardBody>
                                <CardTitle><b>Mission Details</b></CardTitle>
                                <CardSubtitle><b>Mission Id:</b> { this.state.missionId } </CardSubtitle>
                                <CardText><p>  <b>Astronauts :</b> { this.state.secondAstronautsGuId} and  { this.state.firstAstronautsGuId}</p> <h4>Have a safe Journey!  </h4></CardText>
                                <Button className=" clearButton" size="sm" block onClick={this.resetState}><h1> Clear</h1></Button>
                            </CardBody>
                        </Card>
                    </div>}
                </div>
                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                    <ModalHeader toggle={this.toggleModal}>Prepare to Launch</ModalHeader>
                    <ModalBody>
                        <LocalForm onSubmit={(values) => this.handleSubmit(values)}>
                            <Row className="form-group">
                                <Label htmlFor="guId" md={12}>Your ID</Label>
                                <Col md={12}>
                                    <Control.text model=".guId" id="guId" name="guId"
                                    placeholder="Enter Id"
                                    className="form-control"
                                    validators={{
                                   required, minLength: minLength(15), maxLength: maxLength(40)
                                   }}
                                    />
                                    <Errors
                                        className="text-danger"
                                        model=".guId"
                                        show="touched"
                                        messages={{
                                            required: 'Required',
                                            minLength: 'Must be greater than 15 characters',
                                            maxLength: 'Must be 40 characters or less'
                                    }}
                                    />
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Col md={{size:10, offset: 0}}>
                                    <Button type="submit" className="submitButton" value="submit" color="primary">
                                        Submit
                                    </Button>
                                </Col>
                            </Row>
                        </LocalForm>
                    </ModalBody>
                </Modal>
            </div>
        );
    }

}

function Home(){
    return (
        <div className="homeContainer ">
            <div className='row row-header'>
                <div className='col-12 col-sm-6 text-center m-auto' >
                    <h1>Feel the Speed!</h1>
                    <h3>Examining the workhorses supplying humanity with tools and resources needed for life off Earth.</h3>
                </div>
            </div>

            <div className="homeFooter">
                <div className="row align-items-center">
                    <div className="col-md-4 col-sm-4 align-items-center"></div>
                    <div className="col-md-4 col-sm-4 align-items-center"> <CommentForm /></div>
                    <div className="col-md-4 col-sm-4"></div>
                </div>

                <div className="row row-content">
                    <div className="col-md-5  m-auto col-sm-12 align-items-center">
                        <h2> Don't Take a Truck </h2>
                    </div>
                    <div className="col-md-5 col-md m-auto col-sm-12 align-items-center">
                        <h2> Take a Truck </h2>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Home;