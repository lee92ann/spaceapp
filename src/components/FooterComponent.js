import React from 'react';

function Footer() {
    return(
        <div className="footer">
            <div className="row justify-content-center ">
                <div className="col-md-auto ">
                    <h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry</h4>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                </div>
            </div>

            <div className="row justify-content-center">
                <div className="col-auto">
                    <p>© Copyright 2019 Full Tilt</p>
                </div>
            </div>
        </div>
    )
}

export default Footer;

