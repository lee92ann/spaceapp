import React, { Component } from 'react';
import Header from './HeaderComponent';
import Home from './HomeComponent';
import Footer from './FooterComponent';
import { Switch, Route, Redirect} from 'react-router-dom'

class MainComponent extends Component {

    render() {

    const HomePage = () => {
        return (
            <Home/>
        );
    }
        return (
            <div>
                <Header />
                    <Switch>
                        <Route path ="/home" component = {HomePage} />
                        <Redirect to="/home" />
                    </Switch>
                <Footer />
            </div>
        );
    }
}


export default MainComponent;
