const express = require('express');
const app = express();
const port = process.env.PORT || 5000;

var users = require('./users.json');

var dog = "488fb5c8-861d-48a6-a9a6-b315a6801116";
var availableAstr = [];
var fs = require('fs');
var x;

function getAssignedAstronauts(){
    var missions = require('./missions.json');
    var assignedAstronauts =[];
    missions.assignments.filter(function(assignment){
        if(assignment.firstAstronautsGuId !== ""){
           assignedAstronauts.push(assignment.firstAstronautsGuId)
        } if (assignment.secondAstronautsGuId !== ""){
            assignedAstronauts.push(assignment.secondAstronautsGuId)
        }
    });
    return assignedAstronauts;
}

function getUnassignedAstronauts(){
    var allAstronauts = [];
    var assignedAstronauts = getAssignedAstronauts();
    var unassignedAstronauts =[];

    users.astronauts.filter(function(user){
          if(user.guid != dog){
             allAstronauts.push(user.guid);
          }
    });
    unassignedAstronauts = allAstronauts.filter(x => !assignedAstronauts.includes(x));

    return unassignedAstronauts;
}
function isRegisteredAstronaut(guid){
    var x = false;
    var astronaut = users.astronauts.filter(function(user){
        if(user.guid == guid){
            x = true;
        }
    });
    return x;
}



function removeFromArray(arr, el){
    for( var i = 0; i < arr.length; i++){
       if ( arr[i] === el) {
         arr.splice(i, 1);
       }
    }
    return arr;

}
function getMissionDetails(guid){
    var currentMission = require('./missions.json');
    var details =(currentMission.assignments);
    var result = {} ;

    for(var i =0; i < details.length; i++){
        if(details[i].firstAstronautsGuId == guid){
        result = details[i];
        } if (details[i].secondAstronautsGuId == guid){
        result = details[i];
        }
    }
    return result;

}

function assignedAstronautsToMission(guid){

var missions = require('./missions.json');
var newMission;
    fs.readFile('./missions.json', 'utf8', function readFileCallback(err, data){
        if (err){
            console.log(err);
        } else {
            var missionId = missions.assignments.length;
            var unassigned = getUnassignedAstronauts();
            removeFromArray(unassigned, guid)
            var secondAstro = unassigned.pop();

            obj = JSON.parse(data); //now it an object
            obj.assignments.push( {
                 "missionId": missionId + 1 ,
                 "firstAstronautsGuId": guid,
                 "secondAstronautsGuId": secondAstro
             }); //add some data
             x  = {
              "missionId": missionId + 1 ,
              "firstAstronautsGuId": guid,
              "secondAstronautsGuId": secondAstro
            }
            json = JSON.stringify(obj); //convert it back to json
            fs.writeFile('./missions.json', json, 'utf8', function (err) {
                if (err) throw err;
                console.log('Saved!');
            });
        }
    });

}

app.listen(port, () => console.log(`Listening on port ${port}`));


app.get('/express_backend', (req, res) => {
  res.send({ express: 'YOUR EXPRESS BACKEND IS CONNECTED TO REACT' });
});


app.get('/astronauts', (req, res) => {
    res.json(users);
});

app.get('/unassignedastronauts', (req, res)=>{
    var x = getUnassignedAstronauts();
    JSON.stringify(x);
    res.json(x);
})

app.get('/astronauts/:guid', (req, res) => {
    var astronaut = users.astronauts.filter(function(user){
      if(user.guid == req.params.guid){
         return true;
      }
   });
   if(astronaut.length == 1){
      res.json(astronaut[0])
   } else {
      res.status(404);
      res.json({message: "Not Found"});
   }
});

app.get('/missions', (req, res) => {
var missions = require('./missions.json');
  res.json(missions);
});

app.get('/launch/:guid', (req, res) => {
var missions = require('./missions.json');

    var mission = missions.assignments.filter(function(assignment){
        if(req.params.guid == dog ){
           res.status(404);
           res.json({message: "Problem with request, have you provided the correct GuId?"});
        }
        //check if Astronaut is currently assigned to a Mission.
        else if(assignment.firstAstronautsGuId == req.params.guid || assignment.secondAstronautsGuId == req.params.guid ){
        res.json(assignment);
        }
        else if(isRegisteredAstronaut(req.params.guid) != true) {
           res.status(404);
           res.json({message: "Problem with request, have you provided the correct GuId?"});
        }
        //else Astronaut should be assigned to a Mission.
        else {
            assignedAstronautsToMission(req.params.guid);
            setTimeout(function(){ res.json(x); }, 1000)
        }
    });


});


