module.exports = {
    'Get Mission Details' : function (browser) {
        browser
        .url('http://localhost:3000/')
        .waitForElementVisible('.launchButton')
        .click('.launchButton')
        .waitForElementVisible('.submitButton')
        .setValue('input[type=text]', '4f5606a0-deb6-414b-8013-ef484b0b14be')
        .click('.submitButton')
        .waitForElementVisible('.card-text')
        .assert.containsText('.card-text', '4f5606a0-deb6-414b-8013-ef484b0b14be')
        .end();
    },

    'Validate GuId' : function (browser) {
        browser
        .url('http://localhost:3000/')
        .waitForElementVisible('.launchButton')
        .click('.launchButton')
        .waitForElementVisible('.submitButton')
        .setValue('input[type=text]', '4f5606a')
        .click('.submitButton')
        .assert.containsText('.text-danger', 'Must be greater than 15 characters')
        .setValue('input[type=text]', 'Over40CharactersInserted into text box')
        .click('.submitButton')
        .assert.containsText('.text-danger', 'Must be 40 characters or less')
        .end();
    },

    'Get REST Astronauts GuId end point' : function (browser) {
        browser
        .url('http://localhost:5000/astronauts/4f5606a0-deb6-414b-8013-ef484b0b14be')
        .assert.containsText('body', '4f5606a0-deb6-414b-8013-ef484b0b14be' , 'Yuri', 'Gagarin' )
        .end();
    },

    'Get REST all Astronauts  end point' : function (browser) {
        browser
        .url('http://localhost:5000/astronauts/')
        .assert.containsText('body', '4f5606a0-deb6-414b-8013-ef484b0b14be' , '4ef3b124-bf47-4736-9f0a-3b964baa5b33"', 'd0d4960d-a506-4921-b480-e9cbb76f1017' )
        .end();
    }
};